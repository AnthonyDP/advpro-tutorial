package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MiniDuckSimulator {

    public static void main(String[] args) {
        Duck mallard = new MallardDuck();
        mallard.performQuack();
        mallard.performFly();
        mallard.display();
        mallard.swim();

        Duck model = new ModelDuck();
        model.display();
        model.performQuack();
        model.setQuackBehavior(new Squeak());
        model.performQuack();
        model.performFly();
        model.setFlyBehavior(new FlyRocketPowered());
        model.performFly();
    }
}
