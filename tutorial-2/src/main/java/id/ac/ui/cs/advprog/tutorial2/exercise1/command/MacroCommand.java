package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MacroCommand implements Command {

    private List<Command> commands;

    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        // for (int i = 0; i < commands.size(); i++) {
        //     commands.get(i).execute();
        // }

        commands
            .stream()
            .forEach(Command::execute);
    }

    @Override
    public void undo() {
        // for (int i = (commands.size() - 1); i >= 0; i--) {
        //     commands.get(i).undo();
        // }

        commands
            .stream()
            .collect(Collectors.toCollection(ArrayDeque::new))
            .descendingIterator()
            .forEachRemaining(Command::undo);
    }
}
