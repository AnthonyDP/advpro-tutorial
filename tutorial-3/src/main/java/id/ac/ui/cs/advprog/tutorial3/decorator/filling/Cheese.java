package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cheese extends Food {
    Food food;

    public Cheese(Food food) {
        this.food = food;
        this.description = "cheese";
    }

    @Override
    public String getDescription() {
        return food.getDescription() + ", adding " + this.description;
    }

    @Override
    public double cost() {
        return 2 + food.cost();
    }
}
